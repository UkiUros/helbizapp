package com.helbiz.test

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import com.helbiz.test.helpers.LOCATION_PERMISSIONS_REQUEST
import com.helbiz.test.helpers.isSystemLocationPermissionGranted
import kotlinx.android.synthetic.main.activity_boarding.*

class BoardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isSystemLocationPermissionGranted(this)) {
            moveToMainScreen()
            return
        }

        setContentView(R.layout.activity_boarding)

        buttonNext.setOnClickListener {
            requestLocationPermissionIfNeeded()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSIONS_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    moveToMainScreen()
                } else {
                    finish()
                }

            }
        }

    }

    private fun requestLocationPermissionIfNeeded() {
        ActivityCompat.requestPermissions(
            this@BoardingActivity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            LOCATION_PERMISSIONS_REQUEST
        )
    }

    private fun moveToMainScreen() {
        MainActivity.start(this)
        finish()
    }

    companion object {

        fun start(context: Context) {
            context.startActivity(Intent(context, BoardingActivity::class.java))
        }
    }
}
