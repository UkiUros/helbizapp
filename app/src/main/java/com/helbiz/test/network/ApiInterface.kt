package com.helbiz.test.network

import com.helbiz.test.model.RestaurantInfoResponse
import com.helbiz.test.model.RestaurantResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("poi.json?tag_labels=eatingout&fields=id,name,score,coordinates&count=50&order_by=distance")
    fun fetchRestaurants(
        @Query(value = "annotate", encoded = true) coordinatesFormatted: String,
        @Query("distance", encoded = true) distance: String = "<5000"
    ): Call<RestaurantResponse>

    @GET("poi.json?")
    fun fetchRestaurantInfo(@Query(value = "id") restaurantId: String): Call<RestaurantInfoResponse>
}