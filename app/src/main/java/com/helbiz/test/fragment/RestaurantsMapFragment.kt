package com.helbiz.test.fragment

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager
import com.helbiz.test.DetailActivity
import com.helbiz.test.R
import com.helbiz.test.helpers.ARG_LOCATION
import com.helbiz.test.helpers.ARG_RESTAURANT_LIST
import com.helbiz.test.model.Restaurant


class RestaurantsMapFragment : Fragment(), OnMapReadyCallback,
    ClusterManager.OnClusterItemInfoWindowClickListener<Restaurant> {

    private var restaurants: MutableList<Restaurant> = arrayListOf()

    private lateinit var googleMap: GoogleMap
    private lateinit var clusterManager: ClusterManager<Restaurant>
    private var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            val restaurantList = bundle.getParcelableArrayList<Restaurant>(ARG_RESTAURANT_LIST)
            restaurantList?.let {
                restaurants.addAll(it)
            }

            location = bundle.getParcelable(ARG_LOCATION)

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        location?.let {
            mapFragment.getMapAsync(this)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
            googleMap.uiSettings.isCompassEnabled = true
            googleMap.uiSettings.setAllGesturesEnabled(true)
            googleMap.isMyLocationEnabled = true
            googleMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(location!!.latitude, location!!.longitude), 15f
                )
            )

            addClusterOnMap()
        }
    }

    override fun onClusterItemInfoWindowClick(restaurant: Restaurant?) {
        activity?.let { activityContext ->
            restaurant?.let { DetailActivity.start(activityContext, restaurant.id) }
        }
    }

    private fun addClusterOnMap() {
        if (restaurants.isEmpty()) return

        clusterManager = ClusterManager(activity, googleMap)
        googleMap.setOnCameraIdleListener(clusterManager)
        googleMap.setOnInfoWindowClickListener(clusterManager)
        clusterManager.setOnClusterItemInfoWindowClickListener(this)

        addMarkersToCluster()
        clusterManager.cluster()

    }

    private fun addMarkersToCluster() {
        for (item in restaurants) {
            clusterManager.addItem(item)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(location: Location, restaurantList: ArrayList<Restaurant>) =
            RestaurantsMapFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_RESTAURANT_LIST, restaurantList)
                    putParcelable(ARG_LOCATION, location)
                }
            }
    }
}
