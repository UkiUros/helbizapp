package com.helbiz.test.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.helbiz.test.DetailActivity
import com.helbiz.test.R
import com.helbiz.test.adapter.RestaurantAdapter
import com.helbiz.test.adapter.SimpleDividerItemDecoration
import com.helbiz.test.helpers.ARG_RESTAURANT_LIST
import com.helbiz.test.model.Restaurant
import kotlinx.android.synthetic.main.fragment_restaurants.*


class RestaurantsListFragment : Fragment(), RestaurantAdapter.Callback {

    private var restaurants: MutableList<Restaurant> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            val restaurantList = bundle.getParcelableArrayList<Restaurant>(ARG_RESTAURANT_LIST)
            restaurantList?.let {
                restaurants.addAll(it)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_restaurants, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.addItemDecoration(SimpleDividerItemDecoration(view.context, false))
        recyclerView.adapter = RestaurantAdapter(restaurants, this)
    }

    override fun onItemClick(restaurant: Restaurant) {
        activity?.let { DetailActivity.start(it, restaurant.id) }
    }

    companion object {
        @JvmStatic
        fun newInstance(restaurantList: ArrayList<Restaurant>) =
            RestaurantsListFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_RESTAURANT_LIST, restaurantList)
                }
            }
    }
}
