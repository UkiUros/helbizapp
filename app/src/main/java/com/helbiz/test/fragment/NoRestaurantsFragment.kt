package com.helbiz.test.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.helbiz.test.R


class NoRestaurantsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_no_restaurants, container, false)

    companion object {
        @JvmStatic
        fun newInstance() = NoRestaurantsFragment()
    }
}
