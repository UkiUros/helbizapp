package com.helbiz.test

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.helbiz.test.helpers.ARG_RESTAURANT_ID
import com.helbiz.test.helpers.BASE_URL
import com.helbiz.test.model.RestaurantInfo
import com.helbiz.test.model.RestaurantInfoResponse
import com.helbiz.test.network.ApiInterface
import com.helbiz.test.network.RetrofitClient
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.layout_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailActivity : AppCompatActivity() {

    private var restaurantId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        if (intent.hasExtra(ARG_RESTAURANT_ID)) {
            restaurantId = intent.getStringExtra(ARG_RESTAURANT_ID)
        }

        restaurantId?.let {
            fetchDetails(it)
        }
    }

    private fun fetchDetails(restaurantId: String) {
        showLoadingState(true)
        RetrofitClient.getClient(BASE_URL)
            .create(ApiInterface::class.java)
            .fetchRestaurantInfo(restaurantId)
            .enqueue(object : Callback<RestaurantInfoResponse> {
                override fun onResponse(
                    call: Call<RestaurantInfoResponse>,
                    response: Response<RestaurantInfoResponse>
                ) {

                    showLoadingState(false)
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        responseBody?.let {
                            if (responseBody.restaurants.isNotEmpty()) {
                                populateData(responseBody.restaurants.first())
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<RestaurantInfoResponse>, t: Throwable) {
                    Log.d("onFailure", t.message ?: "")
                    showLoadingState(false)
                }

            })
    }

    private fun showLoadingState(isLoading: Boolean) {
        if (isLoading) {
            loadingLayout.visibility = View.VISIBLE
            contentLayout.visibility = View.INVISIBLE
        } else {
            loadingLayout.visibility = View.GONE
            contentLayout.visibility = View.VISIBLE
        }
    }

    private fun populateData(restaurantInfo: RestaurantInfo) {
        nameTextView.text = restaurantInfo.name
        descriptionTextView.text = restaurantInfo.snippet
        ratingTextView.text = restaurantInfo.getScoreFormatted()


        if (!isFinishing) {
            Glide.with(this).load(restaurantInfo.getHeaderImageUrl())
                .thumbnail(1.0f)
                .placeholder(R.mipmap.ic_launcher)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .centerCrop()
                .into(headerImageView)
        }
    }

    companion object {
        fun start(context: Context, restaurantId: String) {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(ARG_RESTAURANT_ID, restaurantId)
            context.startActivity(intent)
        }
    }
}
