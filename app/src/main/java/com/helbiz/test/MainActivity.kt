package com.helbiz.test

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.helbiz.test.fragment.*
import com.helbiz.test.helpers.*
import com.helbiz.test.model.Restaurant
import com.helbiz.test.model.RestaurantResponse
import com.helbiz.test.network.ApiInterface
import com.helbiz.test.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private var restaurants: MutableList<Restaurant> = arrayListOf()

    private lateinit var location: Location
    private val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!isSystemLocationPermissionGranted(this)) {
            goToOnBoarding()
            return
        }

        setLoadingAnimations()
        fetchLocation()
    }

    override fun onResume() {
        super.onResume()
        if (!isSystemLocationPermissionGranted(this)) {
            goToOnBoarding()
        }
    }

    @SuppressLint("MissingPermission")
    private fun fetchLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    this.location = location
                    fetchRestaurants(DISTANCE)
                }
            }
    }

    private fun fetchRestaurants(distance: Int) {
        // recursive call, do not allow infinite loop
        if (distance >= MAX_DISTANCE) {
            setNoRestaurantsState()
            return
        }
        RetrofitClient.getClient(BASE_URL)
            .create(ApiInterface::class.java)
            .fetchRestaurants(getAnnotatedLocation(location), distance.formatDistanceParam())
            .enqueue(object : Callback<RestaurantResponse> {
                override fun onResponse(call: Call<RestaurantResponse>, response: Response<RestaurantResponse>) {
                    if (response.isSuccessful) {
                        restaurants.clear()

                        val responseBody = response.body()
                        responseBody?.let {
                            if (responseBody.restaurants.isEmpty()) {
                                fetchRestaurants(distance + DISTANCE)
                                return
                            }

                            restaurants.addAll(it.restaurants)
                            setDataFragments()
                        }
                    }
                }

                override fun onFailure(call: Call<RestaurantResponse>, t: Throwable) {
                    Log.d("onFailure", t.message ?: "")
                    setNoRestaurantsState()
                }

            })
    }

    private fun setFragments(fragmentTop: Fragment, fragmentBottom: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.frameTop, fragmentTop)
            .replace(R.id.frameBottom, fragmentBottom)
            .commit()
    }

    private fun setDataFragments() {
        setFragments(
            RestaurantsMapFragment.newInstance(location, ArrayList(restaurants)),
            RestaurantsListFragment.newInstance(ArrayList(restaurants))
        )
    }

    private fun setLoadingAnimations() {
        setFragments(
            LoadingMapFragment.newInstance(),
            LoadingListFragment.newInstance()
        )
    }

    private fun setNoRestaurantsState() {
        setFragments(
            RestaurantsMapFragment.newInstance(location, ArrayList(restaurants)),
            NoRestaurantsFragment.newInstance()
        )
    }

    private fun goToOnBoarding() {
        BoardingActivity.start(this)
        finish()
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }

}

/*
    Developer by Uros S.
    email: simic.uros@gmail.com
 */
