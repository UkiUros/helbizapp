package com.helbiz.test.helpers

import android.text.TextUtils

/**
 * Created by urossimic on 2019-08-14.
 **/

fun Int.formatDistance(): String {

    if (this < 1000) {
        return TextUtils.concat(this.toString(), " m").toString()
    }

    val distance: Double = (this / 1000).toDouble()
    return TextUtils.concat(String.format("%.1f", distance), " km").toString()
}

fun Int.formatDistanceParam(): String {
    return TextUtils.concat("<", this.toString()).toString()
}