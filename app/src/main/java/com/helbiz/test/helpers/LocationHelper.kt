package com.helbiz.test.helpers

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.text.TextUtils
import androidx.core.app.ActivityCompat

const val LOCATION_PERMISSIONS_REQUEST: Int = 100

fun isSystemLocationPermissionGranted(context: Context): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    return true
}

fun getAnnotatedLocation(location: Location): String {
    return TextUtils.concat(
        "distance:",
        location.latitude.toString(),
        ",",
        location.longitude.toString()
    ).toString()
}
