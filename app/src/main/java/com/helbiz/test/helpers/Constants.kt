package com.helbiz.test.helpers

const val BASE_URL = "https://www.triposo.com/api/20181213/"

const val ARG_RESTAURANT_LIST = "restaurant_list"
const val ARG_LOCATION = "location"
const val ARG_RESTAURANT_ID = "restaurant_id"

const val DISTANCE: Int = 5000
const val MAX_DISTANCE = 10 * DISTANCE