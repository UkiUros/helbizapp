package com.helbiz.test.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.helbiz.test.R
import com.helbiz.test.helpers.formatDistance
import com.helbiz.test.model.Restaurant

class RestaurantAdapter(
    private val restaurantList: List<Restaurant>,
    private val callback: Callback
) : RecyclerView.Adapter<RestaurantHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantHolder {
        return RestaurantHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_restaurant, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return restaurantList.size
    }

    override fun onBindViewHolder(holder: RestaurantHolder, position: Int) {
        val restaurant = restaurantList[position]

        holder.restaurantNameTextView.text = restaurant.name
        holder.distanceTextView.text = restaurant.distance.formatDistance()

        holder.itemView.setOnClickListener { callback.onItemClick(restaurant) }
    }


    interface Callback {
        fun onItemClick(restaurant: Restaurant)
    }
}

class RestaurantHolder(view: View) : RecyclerView.ViewHolder(view) {
    val restaurantNameTextView: TextView = view.findViewById(R.id.nameTextView)
    val distanceTextView: TextView = view.findViewById(R.id.distanceTextView)
}