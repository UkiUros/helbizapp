package com.helbiz.test.adapter

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.helbiz.test.R

class SimpleDividerItemDecoration(
    context: Context,
    showDividerAfterLast: Boolean
) : RecyclerView.ItemDecoration() {

    private val mDivider: Drawable?
    private val sideMargin: Int
    private var showDividerAfterLast = true

    init {
        mDivider = ContextCompat.getDrawable(context, R.drawable.line_divider)
        sideMargin = context.resources.getDimensionPixelSize(R.dimen.activity_horizontal_margin)
        this.showDividerAfterLast = showDividerAfterLast
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft + sideMargin
        val right = parent.width - parent.paddingRight - sideMargin

        var childCount = parent.childCount
        if (!showDividerAfterLast) {
            childCount -= 1
        }
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin
            val bottom = top + mDivider!!.intrinsicHeight

            mDivider.setBounds(left, top, right, bottom)
            mDivider.draw(c)
        }
    }
}
