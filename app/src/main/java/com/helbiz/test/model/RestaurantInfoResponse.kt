package com.helbiz.test.model

import com.google.gson.annotations.SerializedName

data class RestaurantInfoResponse(
    @SerializedName("results")
    val restaurants: List<RestaurantInfo>,
    @SerializedName("estimated_total")
    val estimatedTotal: Int,
    @SerializedName("more")
    val more: Boolean
)