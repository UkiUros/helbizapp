package com.helbiz.test.model

import android.os.Parcelable
import android.text.TextUtils
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.google.maps.android.clustering.ClusterItem
import com.helbiz.test.helpers.formatDistance
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Restaurant(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("distance")
    val distance: Int,
    @SerializedName("coordinates")
    val coordinates: Coordinates,
    @SerializedName("score")
    val score: Double,
    @SerializedName("eatingout_score")
    val eatingOutScore: Double
) : Parcelable, ClusterItem {

    override fun getSnippet(): String {
        return distance.formatDistance()
    }

    override fun getTitle(): String {
        return name
    }

    override fun getPosition(): LatLng {
        return LatLng(coordinates.latitude, coordinates.longitude)
    }

}

@Parcelize
data class Coordinates(
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double
) : Parcelable

