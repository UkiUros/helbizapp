package com.helbiz.test.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RestaurantInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("snippet")
    val snippet: String,
    @SerializedName("score")
    val score: Double,
    @SerializedName("images")
    val images: images
) : Parcelable {

    fun getScoreFormatted(): String {
        return String.format("%.2f", score)
    }

    fun getHeaderImageUrl(): String {
        if (images.isNotEmpty()) {
            return images.first().size.originalSize.imageUrl
        }
        return ""
    }
}

typealias images = List<RestaurantImage>

@Parcelize
data class RestaurantImage(@SerializedName("sizes") val size: ImageSize) : Parcelable

@Parcelize
data class ImageSize(@SerializedName("original") val originalSize: OriginalSize) : Parcelable

@Parcelize
data class OriginalSize(@SerializedName("url") val imageUrl: String) : Parcelable