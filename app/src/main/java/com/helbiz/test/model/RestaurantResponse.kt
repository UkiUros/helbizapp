package com.helbiz.test.model

import com.google.gson.annotations.SerializedName

data class RestaurantResponse(
    @SerializedName("results")
    val restaurants: List<Restaurant>,
    @SerializedName("estimated_total")
    val estimatedTotal: Int,
    @SerializedName("more")
    val more: Boolean
)